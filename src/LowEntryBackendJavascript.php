<?php

namespace LowEntryBackendJavascript;


class LowEntryBackendJavascript
{
	const PRODUCTION_SERVER = 'https://backend.lowentry.com';
	const DEVELOPMENT_SERVER = 'https://dev-backend.lowentry.com';
}
