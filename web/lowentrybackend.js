var _internal_LowEntryBackend_showFileDialog_lastId = null;
var LowEntryBackend = window.LowEntryBackend = (function()
{
	/** @type {string} */
	LowEntryBackend.PRODUCTION_SERVER = 'https://backend.lowentry.com';
	/** @type {string} */
	LowEntryBackend.DEVELOPMENT_SERVER = 'https://dev-backend.lowentry.com';
	
	
	var COOKIE_PREFIX_PRODUCTION = 'LowEntryBackend';
	var COOKIE_PREFIX_DEVELOPMENT = 'LowEntryBackendDev';
	var COOKIE_PREFIX_OTHER = 'LowEntryBackendCustom';
	
	
	/** @type {{MISSING_DATA: number, USER_LOGIN_USERNAME_DOESNT_EXIST: number, USER_LOGIN_INCORRECT_PASSWORD: number, CUSTOM_ERROR: number, USER_REGISTER_USERNAME_INVALID: number, INVALID_SERVER_TOKEN: number, USER_UNAUTHORIZED_ACCESS: number, SERVER_TEMPORARILY_UNAVAILABLE: number, SERVER_UNAVAILABLE: number, USER_REGISTER_EMAIL_INVALID: number, OTHER: number, INVALID_CODE: number, SERVER_DISABLED: number, INVALID_DATA: number, USER_REGISTER_USERNAME_ALREADY_IN_USE: number}} */
	LowEntryBackend.ErrorCode = {
		OTHER:                                1,
		CUSTOM_ERROR:                         2,
		MISSING_DATA:                         3,
		INVALID_DATA:                         4,
		INVALID_CODE:                         5,
		INVALID_SERVER_TOKEN:                 6,
		SERVER_DISABLED:                      7,
		SERVER_UNAVAILABLE:                   8,
		SERVER_TEMPORARILY_UNAVAILABLE:       9,
		USER_UNAUTHORIZED_ACCESS:             10,
		USER_LOGIN_USERNAME_DOESNT_EXIST:     11,
		USER_LOGIN_INCORRECT_PASSWORD:        12,
		USER_REGISTER_USERNAME_ALREADY_IN_USE:13,
		USER_REGISTER_USERNAME_INVALID:       14,
		USER_REGISTER_EMAIL_INVALID:          15,
	};
	
	
	/** @type {string} */
	LowEntryBackend.prototype.mainServerUrl = '';
	/** @type {string|null} */
	LowEntryBackend.prototype.cookiePrefix = '';
	
	/** @type {string} */
	LowEntryBackend.prototype.gameUrl = '';
	
	/** @type {number} */
	LowEntryBackend.prototype.gameId = 0;
	/** @type {string} */
	LowEntryBackend.prototype.gameToken = '';
	
	/** @type {string} */
	LowEntryBackend.prototype.userId = '';
	/** @type {string} */
	LowEntryBackend.prototype.userToken = '';
	
	/**
	 * @type {WebSocket}
	 * @private
	 */
	LowEntryBackend.prototype._websocketConnection = null;
	/**
	 * @type {LowEntryBackend~___websocketSendCallback[]|[][]}
	 * @private
	 */
	LowEntryBackend.prototype._websocketSendBuffer = [];
	/**
	 * @type {boolean}
	 * @private
	 */
	LowEntryBackend.prototype._websocketConnectionIsOpen = false;
	/**
	 * @type {Object.<string, {message: LowEntryBackend~__onBroadcastOnMessageReceivedCallback, init: LowEntryBackend~__onBroadcastInitializerCallback, _buffer: string[], _success: boolean|null, _firstRun: boolean}[]>}
	 * @private
	 */
	LowEntryBackend.prototype._websocketListeners = {};
	/**
	 * @type {Object.<string, boolean>}
	 * @private
	 */
	LowEntryBackend.prototype._websocketListeningChannels = {};
	
	/**
	 * @type {LowEntryBackend~__onAuthChangeCallback[]}
	 * @private
	 */
	LowEntryBackend.prototype._authchangeListeners = [];
	
	
	var allowCreate = false;
	
	
	/**
	 * @param {string} mainServerUrl
	 * @param {string} cookiePrefix
	 * @param {string} gameUrl
	 * @param {number} gameId
	 * @param {string} gameToken
	 * @param {string} userId
	 * @param {string} userToken
	 * @constructor
	 * @private
	 */
	function LowEntryBackend(mainServerUrl, cookiePrefix, gameUrl, gameId, gameToken, userId, userToken)
	{
		if(!allowCreate)
		{
			throw new Error('To create a LowEntryBackend instance, use LowEntryBackend.connectToServer(gameId, gameToken, function(success, error, server){});');
		}
		allowCreate = false;
		
		this.mainServerUrl = mainServerUrl;
		this.cookiePrefix = cookiePrefix;
		this.gameUrl = gameUrl;
		this.gameId = gameId;
		this.gameToken = gameToken;
		this.userId = userId;
		this.userToken = userToken;
		
		var THIS = this;
		var websocketSendLoop = function(t)
		{
			try
			{
				while(THIS._websocketSendLoop())
				{
				}
			}
			catch(e)
			{
				console.error(e);
			}
			requestAnimationFrame(websocketSendLoop);
		};
		requestAnimationFrame(websocketSendLoop);
	}
	
	
	
	
	/**
	 * @param {object|Array} element
	 * @param {string|number} index
	 * @return {boolean}
	 * @private
	 */
	LowEntryBackend._hasIndex = function(element, index)
	{
		if(typeof element !== 'object')
		{
			return false;
		}
		if(element === null)
		{
			return false;
		}
		return (index in element);
	};
	
	/**
	 * @callback LowEntryBackend~__eachCallback
	 * @param {*} index
	 * @param {*} value
	 */
	/**
	 * @param {*[]|object|Function} elements
	 * @param {LowEntryBackend~__eachCallback} callback
	 * @param {boolean} [optionalSkipHasOwnPropertyCheck]
	 * @return {*[]|object|Function}
	 * @private
	 */
	LowEntryBackend._each = function(elements, callback, optionalSkipHasOwnPropertyCheck = false)
	{
		if((elements !== null) && (typeof elements !== 'undefined'))
		{
			var index;
			if(Array.isArray(elements))
			{
				for(index = 0; index < elements.length; index++)
				{
					if(callback.call(elements[index], index, elements[index]) === false)
					{
						break;
					}
				}
			}
			else if((typeof elements === 'object') || (typeof elements === 'function'))
			{
				for(index in elements)
				{
					if((optionalSkipHasOwnPropertyCheck === true) || Object.prototype.hasOwnProperty.call(elements, index))
					{
						if(callback.call(elements[index], index, elements[index]) === false)
						{
							break;
						}
					}
				}
			}
			else
			{
				console.warn('Executed LowEntryBackend._each() on an invalid type: [' + (typeof elements) + ']', elements);
			}
		}
		return elements;
	};
	
	
	
	
	/**
	 * @typedef {object} LowEntryBackend~__error
	 * @property {string} name
	 * @property {string} message
	 * @property {number} code
	 * @property {Array} trace
	 */
	/**
	 * @param {string} message
	 * @param {number} code
	 * @param {Array} [trace]
	 * @return {LowEntryBackend~__error}
	 */
	LowEntryBackend.createError = function(message, code, trace)
	{
		var error = {};
		{
			error.name = 'ApiException';
			error.message = message;
			error.code = code;
			error.trace = ((trace === null) || (typeof trace === 'undefined')) ? [] : trace;
		}
		return error;
	};
	
	/**
	 * @param {object|LowEntryBackend~__error} err
	 * @return {LowEntryBackend~__error|object}
	 */
	LowEntryBackend.convertError = function(err)
	{
		var error = {};
		var errType = '' + err;
		if((errType !== '[object Array]') && (errType !== '[object Object]'))
		{
			error.name = 'UnexpectedError';
			error.message = err;
			error.code = LowEntryBackend.ErrorCode.OTHER;
			error.trace = [];
			return error;
		}
		if(LowEntryBackend._hasIndex(err, 'code'))
		{
			if(!LowEntryBackend._hasIndex(err, 'trace'))
			{
				err.trace = [];
			}
			return err;
		}
		error.name = err.name;
		error.message = err.message;
		error.code = LowEntryBackend.ErrorCode.OTHER;
		error.trace = LowEntryBackend._hasIndex(err, 'trace') ? err.trace : [];
		return error;
	};
	
	
	
	
	/**
	 * @return {boolean}
	 */
	LowEntryBackend.prototype.isAuthenticated = function()
	{
		return (this.userId !== '');
	};
	
	/**
	 *
	 */
	LowEntryBackend.prototype.clearAuthentication = function()
	{
		var changed = (this.userId !== '');
		this.userId = '';
		this.userToken = '';
		LowEntryBackend._removeAuthenticationSavegame(this.cookiePrefix, this.gameId);
		if(changed)
		{
			this._authchangeCallbacksEmit();
		}
	};
	
	/**
	 * @param {string} userId
	 * @param {string} userToken
	 */
	LowEntryBackend.prototype.authenticate = function(userId, userToken)
	{
		var changed = (this.userId !== userId);
		this.userId = userId;
		this.userToken = userToken;
		LowEntryBackend._saveAuthenticationSavegame(this.cookiePrefix, this.gameId, this.userId, this.userToken);
		if(changed)
		{
			this._authchangeCallbacksEmit();
		}
	};
	
	/**
	 * @return {string}
	 */
	LowEntryBackend.prototype.getAuthenticatedUserId = function()
	{
		return this.userId;
	};
	
	/**
	 * @return {string}
	 */
	LowEntryBackend.prototype.getAuthenticatedUserToken = function()
	{
		return this.userToken;
	};
	
	/**
	 * @typedef {object} LowEntryBackend~__authenticatedUserData
	 * @property {string} userId
	 * @property {string} userToken
	 */
	/**
	 * @return {LowEntryBackend~__authenticatedUserData|object}
	 */
	LowEntryBackend.prototype.getAuthenticatedUserData = function()
	{
		return {userId:this.userId, userToken:this.userToken};
	};
	
	
	/**
	 * @callback LowEntryBackend~__authenticationChangedCallback
	 * @param {LowEntryBackend} server
	 * @param {string} givenUserId
	 * @param {boolean} givenUserToken
	 * @param {string} newAuthenticated
	 * @param {string|null} newUserId
	 * @param {string|null} newUserToken
	 */
	/**
	 * @param {LowEntryBackend} server
	 * @param {string} givenUserId
	 * @param {boolean} givenUserToken
	 * @param {string} newAuthenticated
	 * @param {string|null} newUserId
	 * @param {string|null} newUserToken
	 * @private
	 */
	LowEntryBackend.prototype._authenticationChangedCallback = function(server, givenUserId, givenUserToken, newAuthenticated, newUserId, newUserToken)
	{
		if(!server.isAuthenticated() || ((String(givenUserId) === String(server.userId)) && (String(givenUserToken) === String(server.userToken))))
		{
			if(!newAuthenticated)
			{
				server.clearAuthentication();
			}
			else
			{
				server.authenticate(newUserId, newUserToken);
			}
		}
	};
	
	
	/**
	 * @return {string}
	 * @private
	 */
	LowEntryBackend.prototype._getGameUrlHostName = function()
	{
		var parser = document.createElement('a');
		parser.href = this.gameUrl;
		return parser.hostname;
	};
	
	
	/**
	 * @callback LowEntryBackend~__connectToServerCallback
	 * @param {boolean} success
	 * @param {LowEntryBackend~__error|null} error
	 * @param {LowEntryBackend|null} server
	 */
	/**
	 * @param {string} mainServerUrl
	 * @param {number} gameId
	 * @param {string} gameToken
	 * @param {LowEntryBackend~__connectToServerCallback} callback (boolean success, object error, LowEntryBackend server)
	 */
	LowEntryBackend.connectToServer = function(mainServerUrl, gameId, gameToken, callback)
	{
		var cookiePrefix = null;
		var urlPrefix = 'local-';
		var portSuffix = ':8001';
		if(mainServerUrl === LowEntryBackend.PRODUCTION_SERVER)
		{
			cookiePrefix = COOKIE_PREFIX_PRODUCTION;
			urlPrefix = '';
			portSuffix = '';
		}
		else if(mainServerUrl === LowEntryBackend.DEVELOPMENT_SERVER)
		{
			cookiePrefix = COOKIE_PREFIX_DEVELOPMENT;
			urlPrefix = 'dev-';
			portSuffix = '';
		}
		
		var gameUrl = 'https://' + urlPrefix + gameId + '.game-backend-lowentry.com' + portSuffix;
		
		var loadAuth = LowEntryBackend._loadAuthenticationSavegame(cookiePrefix, gameId);
		var userId = '';
		var userToken = '';
		if(loadAuth.success)
		{
			userId = loadAuth.userId;
			userToken = loadAuth.userToken;
		}
		
		allowCreate = true;
		var instance = new LowEntryBackend(mainServerUrl, cookiePrefix, gameUrl, gameId, gameToken, userId, userToken);
		
		var startTime = performance.now();
		var attempt = function()
		{
			LowEntryBackend._executeConnectToServerRequest(instance, gameUrl, gameId, gameToken, userId, userToken, instance._authenticationChangedCallback, function(success, error)
			{
				if(success)
				{
					callback(true, null, instance);
					return;
				}
				if((error.code !== LowEntryBackend.ErrorCode.SERVER_TEMPORARILY_UNAVAILABLE) || (((performance.now() - startTime) / 1000.0) > 60))
				{
					callback(false, error, null);
					return;
				}
				console.warn('game server is temporarily unavailable, retrying connect');
				setTimeout(attempt, 5 * 1000);
			});
		};
		attempt();
	};
	
	/**
	 * @param {string} mainServerUrl
	 * @param {number} gameId
	 * @param {string} gameToken
	 * @return {Promise<*>}
	 */
	LowEntryBackend.connectToServerPromise = function(mainServerUrl, gameId, gameToken)
	{
		return new Promise(function(resolve, reject)
		{
			try
			{
				LowEntryBackend.connectToServer
				(
					mainServerUrl,
					gameId,
					gameToken,
					function(success, error, server)
					{
						if(success)
						{
							resolve(server);
							return;
						}
						reject(error);
					},
				);
			}
			catch(e)
			{
				reject(LowEntryBackend.convertError(e));
			}
		});
	};
	
	
	/**
	 * @callback LowEntryBackend~__apiCallCallback
	 * @param {boolean} success
	 * @param {LowEntryBackend~__error|null} error
	 * @param {object|null} OUT
	 */
	/**
	 * @param {string} name
	 * @param {object} IN
	 * @param {LowEntryBackend~__apiCallCallback} callback (boolean success, object error, object OUT)
	 */
	LowEntryBackend.prototype.apiCall = function(name, IN, callback)
	{
		var THIS = this;
		var startTime = performance.now();
		var attempt = function()
		{
			LowEntryBackend._executeApiCallRequest(THIS, THIS.gameUrl, THIS.gameId, THIS.gameToken, THIS.userId, THIS.userToken, name, IN, THIS._authenticationChangedCallback, function(success, error, OUT)
			{
				if(success)
				{
					callback(true, null, OUT);
					return;
				}
				if((error.code !== LowEntryBackend.ErrorCode.SERVER_TEMPORARILY_UNAVAILABLE) || (((performance.now() - startTime) / 1000.0) > 60))
				{
					callback(false, error, null);
					return;
				}
				console.warn('game server is temporarily unavailable, retrying endpoint: ' + name);
				setTimeout(attempt, 5 * 1000);
			});
		};
		attempt();
	};
	
	/**
	 * @param {string} name
	 * @param {object} [IN]
	 * @return {Promise<*>}
	 */
	LowEntryBackend.prototype.apiCallPromise = function(name, IN)
	{
		if((typeof IN === 'undefined') || (IN === null))
		{
			IN = {};
		}
		return new Promise(function(resolve, reject)
		{
			try
			{
				this.apiCall
				(
					name,
					IN,
					function(success, error, OUT)
					{
						if(success)
						{
							resolve(OUT);
							return;
						}
						reject(error);
					},
				);
			}
			catch(e)
			{
				reject(LowEntryBackend.convertError(e));
			}
		});
	};
	
	
	/**
	 * @callback LowEntryBackend~__onBroadcastOnMessageReceivedCallback
	 * @param {*} message
	 */
	/**
	 * @callback LowEntryBackend~__onBroadcastInitializerResolveCallback
	 * @param {*} [value]
	 */
	/**
	 * @callback LowEntryBackend~__onBroadcastInitializerRejectCallback
	 * @param {*} [reason]
	 */
	/**
	 * @callback LowEntryBackend~__onBroadcastInitializerCallback
	 * @param {LowEntryBackend~__onBroadcastInitializerResolveCallback} resolve
	 * @param {LowEntryBackend~__onBroadcastInitializerRejectCallback} reject
	 * @param {boolean} [firstRun]
	 */
	/**
	 * @param {string} channel
	 * @param {LowEntryBackend~__onBroadcastOnMessageReceivedCallback} onMessageReceivedCallback (message)
	 * @param {LowEntryBackend~__onBroadcastInitializerCallback|null} [initializerCallback] (function resolve, function reject, boolean firstRun)
	 * @return {object}
	 */
	LowEntryBackend.prototype.onbroadcast = function(channel, onMessageReceivedCallback, initializerCallback)
	{
		var key = 's' + this.gameId + '_' + channel;
		var listener = this._websocketOnBroadcast(key, onMessageReceivedCallback, initializerCallback);
		return (function()
		{
			/**
			 * @constructor
			 * @private
			 */
			function result()
			{
			}
			
			result.remove = function()
			{
				this._websocketOffBroadcast(key, listener);
			};
			
			return result;
		})();
	};
	
	/**
	 * @param {string} channel
	 */
	LowEntryBackend.prototype.offbroadcast = function(channel)
	{
		var key = 's' + this.gameId + '_' + channel;
		this._websocketOffBroadcast(key);
	};
	
	/**
	 *
	 */
	LowEntryBackend.prototype.offbroadcastall = function()
	{
		this._websocketOffAllBroadcasts('s');
	};
	
	
	/**
	 * @param {string} channel
	 * @param {LowEntryBackend~__onBroadcastOnMessageReceivedCallback} onMessageReceivedCallback (message)
	 * @param {LowEntryBackend~__onBroadcastInitializerCallback|null} [initializerCallback] (function resolve, function reject, boolean firstRun)
	 * @return {object}
	 */
	LowEntryBackend.prototype.onclientbroadcast = function(channel, onMessageReceivedCallback, initializerCallback)
	{
		var key = 'c' + this.gameId + '_' + channel;
		var listener = this._websocketOnBroadcast(key, onMessageReceivedCallback, initializerCallback);
		return (function()
		{
			/**
			 * @constructor
			 * @private
			 */
			function result()
			{
			}
			
			result.remove = function()
			{
				this._websocketOffBroadcast(key, listener);
			};
			
			return result;
		})();
	};
	
	/**
	 * @param {string} channel
	 */
	LowEntryBackend.prototype.offclientbroadcast = function(channel)
	{
		var key = 'c' + this.gameId + '_' + channel;
		this._websocketOffBroadcast(key);
	};
	
	/**
	 *
	 */
	LowEntryBackend.prototype.offclientbroadcastall = function()
	{
		this._websocketOffAllBroadcasts('c');
	};
	
	
	/**
	 * @param {string} channel
	 * @param {*} message
	 */
	LowEntryBackend.prototype.clientbroadcast = function(channel, message)
	{
		channel = '' + channel;
		if(channel.length > 256)
		{
			console.error('in clientbroadcast(), the given channel name is longer than 256 characters long, please choose a different channel name');
			return;
		}
		
		message = JSON.stringify({'result':message});
		if(message.length > 10485760)
		{
			console.error('in clientbroadcast(), the given message (after json encoding it) is larger than the maximum allowed size of 10 MB (10485760 bytes)');
			return;
		}
		
		var key = 'c' + this.gameId + '_' + channel;
		
		try
		{
			var connection = this._websocketGetConnection();
			if(connection === null)
			{
				return;
			}
			var keyBytes = LowEntryBackend._stringToBytesUtf8(key);
			var messageBytes = LowEntryBackend._stringToBytesUtf8(message);
			this._websocketSend([].concat(LowEntryBackend._uint8ToBytes(1), LowEntryBackend._int32ToBytes(keyBytes.length), keyBytes, LowEntryBackend._int32ToBytes(messageBytes.length), messageBytes));// 1 = BROADCAST
		}
		catch(e)
		{
			console.error(e);
		}
	};
	
	
	/**
	 * @callback LowEntryBackend~__onAuthChangeCallback
	 * @param {LowEntryBackend} [server]
	 */
	/**
	 * @param {LowEntryBackend~__onAuthChangeCallback} callback ([LowEntryBackend server])
	 * @return {object}
	 */
	LowEntryBackend.prototype.onauthchange = function(callback)
	{
		this._authchangeListeners.push(callback);
		
		return (function()
		{
			/**
			 * @constructor
			 * @private
			 */
			function result()
			{
			}
			
			result.remove = function()
			{
				this._offauthchangecallback(callback);
			};
			
			return result;
		})();
	};
	
	/**
	 * @param {LowEntryBackend~__onAuthChangeCallback} callback ([LowEntryBackend server])
	 * @private
	 */
	LowEntryBackend.prototype._offauthchangecallback = function(callback)
	{
		while(true)
		{
			var index = this._authchangeListeners.indexOf(callback);
			if(index <= -1)
			{
				break;
			}
			this._authchangeListeners.splice(index, 1);
			if(this._authchangeListeners.length === 0)
			{
				break;
			}
		}
	};
	
	/**
	 *
	 */
	LowEntryBackend.prototype.offauthchangeall = function()
	{
		this._authchangeListeners = [];
	};
	
	/**
	 * @private
	 */
	LowEntryBackend.prototype._authchangeCallbacksEmit = function()
	{
		var authchangeListenersClone = this._authchangeListeners.slice(0);
		LowEntryBackend._each(authchangeListenersClone, function(index, callback)
		{
			try
			{
				callback(this);
			}
			catch(e)
			{
				console.error(e);
			}
		});
	};
	
	
	
	
	/**
	 * @typedef {object} LowEntryBackend~__loadAuth
	 * @property {boolean} success
	 * @property {string|undefined} [userId]
	 * @property {string|undefined} [userToken]
	 */
	/**
	 * @param {string|null} cookiePrefix
	 * @param {number} gameId
	 * @return {LowEntryBackend~__loadAuth}
	 * @private
	 */
	LowEntryBackend._loadAuthenticationSavegame = function(cookiePrefix, gameId)
	{
		if(cookiePrefix === null)
		{
			cookiePrefix = COOKIE_PREFIX_OTHER;
		}
		var data = {};
		data.success = false;
		var result = window.localStorage.getItem(cookiePrefix + '_ServerAuth_' + gameId);
		if((typeof result === 'undefined') || (result === null))
		{
			return data;
		}
		try
		{
			result = JSON.parse(result);
		}
		catch(e)
		{
			return data;
		}
		if((typeof result === 'undefined') || (result === null) || !LowEntryBackend._hasIndex(result, 'userId') || !LowEntryBackend._hasIndex(result, 'userToken'))
		{
			return data;
		}
		data.success = true;
		data.userId = result.userId;
		data.userToken = result.userToken;
		return data;
	};
	
	/**
	 * @param {string|null} cookiePrefix
	 * @param {number} gameId
	 * @param {string} userId
	 * @param {string} userToken
	 * @private
	 */
	LowEntryBackend._saveAuthenticationSavegame = function(cookiePrefix, gameId, userId, userToken)
	{
		if(cookiePrefix === null)
		{
			cookiePrefix = COOKIE_PREFIX_OTHER;
		}
		window.localStorage.setItem(cookiePrefix + '_ServerAuth_' + gameId, JSON.stringify({userId:userId, userToken:userToken}));
	};
	
	/**
	 * @param {string|null} cookiePrefix
	 * @param {number} gameId
	 * @private
	 */
	LowEntryBackend._removeAuthenticationSavegame = function(cookiePrefix, gameId)
	{
		if(cookiePrefix === null)
		{
			cookiePrefix = COOKIE_PREFIX_OTHER;
		}
		window.localStorage.removeItem(cookiePrefix + '_ServerAuth_' + gameId);
	};
	
	
	
	
	/**
	 * @param {object} result
	 * @param {string} variable
	 * @return {*}
	 * @private
	 */
	LowEntryBackend._getFromApiResponse = function(result, variable)
	{
		if(!LowEntryBackend._hasIndex(result, variable))
		{
			throw LowEntryBackend.createError('http request failed, the data the server has send back is incomplete, it is missing "' + variable + '"', LowEntryBackend.ErrorCode.OTHER);
		}
		return result[variable];
	};
	
	/**
	 * @param {object} result
	 * @param {string} variable
	 * @return {*}
	 * @private
	 */
	LowEntryBackend._getFromApiResponseResult = function(result, variable)
	{
		if(!LowEntryBackend._hasIndex(result, variable))
		{
			throw LowEntryBackend.createError('http request failed, the data the server has send back is incomplete, it is missing "result"["' + variable + '"]', LowEntryBackend.ErrorCode.OTHER);
		}
		return result[variable];
	};
	
	/**
	 * @param {object} result
	 * @param {string} variable
	 * @return {*}
	 * @private
	 */
	LowEntryBackend._getFromApiResponseResultApiCall = function(result, variable)
	{
		if(!LowEntryBackend._hasIndex(result, variable))
		{
			throw LowEntryBackend.createError('http request failed, the data the server has send back is incomplete, it is missing "result"["apiCall"]["' + variable + '"]', LowEntryBackend.ErrorCode.OTHER);
		}
		return result[variable];
	};
	
	
	/**
	 * @param {object} data
	 * @return {*}
	 * @private
	 */
	LowEntryBackend._parseApiResponse = function(data)
	{
		var success = LowEntryBackend._getFromApiResponse(data, 'success');
		if(success !== true)
		{
			var errorMessage = LowEntryBackend._getFromApiResponse(data, 'errorMessage');
			var errorCode = LowEntryBackend._getFromApiResponse(data, 'errorCode');
			var errorTrace = LowEntryBackend._getFromApiResponse(data, 'errorTrace');
			throw LowEntryBackend.createError(errorMessage, errorCode, errorTrace);
		}
		if(!LowEntryBackend._hasIndex(data, 'result'))
		{
			return {};
		}
		return data.result;
	};
	
	/**
	 * @param {object} data
	 * @return {*}
	 * @private
	 */
	LowEntryBackend._parseApiResponseResultApiCall = function(data)
	{
		var success = LowEntryBackend._getFromApiResponseResultApiCall(data, 'success');
		if(success !== true)
		{
			var errorMessage = LowEntryBackend._getFromApiResponseResultApiCall(data, 'errorMessage');
			var errorCode = LowEntryBackend._getFromApiResponseResultApiCall(data, 'errorCode');
			var errorTrace = LowEntryBackend._getFromApiResponse(data, 'errorTrace');
			throw LowEntryBackend.createError(errorMessage, errorCode, errorTrace);
		}
		if(!LowEntryBackend._hasIndex(data, 'result'))
		{
			return {};
		}
		return data.result;
	};
	
	
	/**
	 * @param {object} res
	 * @param {string} textStatus
	 * @param {string} errorMessage
	 * @private
	 */
	LowEntryBackend._handleApiCallFail = function(res, textStatus, errorMessage)
	{
		if(res.status === 404)
		{
			throw LowEntryBackend.createError('server could not be found, returned 404', LowEntryBackend.ErrorCode.SERVER_UNAVAILABLE);
		}
		throw LowEntryBackend.createError('http request failed, response code ' + res.status + ', error: ' + errorMessage, LowEntryBackend.ErrorCode.OTHER);
	};
	
	
	/**
	 * @callback LowEntryBackend~___executeRequestBaseCallback
	 * @param {boolean} success
	 * @param {LowEntryBackend~__error|null} error
	 * @param {object|null} result
	 */
	/**
	 * @param {string} url
	 * @param {object} data
	 * @param {LowEntryBackend~___executeRequestBaseCallback} callback (boolean success, object error, object result)
	 * @private
	 */
	LowEntryBackend._executeRequestBase = function(url, data, callback)
	{
		try
		{
			$.ajax
			({
				type:    'POST',
				method:  'POST',
				url:     url,
				data:    {json:JSON.stringify(data)},
				dataType:'json',
				cache:   false,
			}).done
			(function(data)
			{
				try
				{
					try
					{
						var result = LowEntryBackend._parseApiResponse(data);
					}
					catch(err)
					{
						callback(false, LowEntryBackend.convertError(err), null);
						return;
					}
					callback(true, null, result);
				}
				catch(e)
				{
					console.error(e);
				}
			}).fail
			(function(res, textStatus, errorMessage)
			{
				try
				{
					try
					{
						LowEntryBackend._handleApiCallFail(res, textStatus, errorMessage);
					}
					catch(err)
					{
						callback(false, LowEntryBackend.convertError(err), null);
					}
				}
				catch(e)
				{
					console.error(e);
				}
			});
		}
		catch(err)
		{
			try
			{
				callback(false, LowEntryBackend.convertError(err), null);
			}
			catch(e)
			{
				console.error(e);
			}
		}
	};
	
	
	/**
	 * @callback LowEntryBackend~___executeConnectToServerRequestCallback
	 * @param {boolean} success
	 * @param {LowEntryBackend~__error|null} error
	 */
	/**
	 * @param {LowEntryBackend} server
	 * @param {string} gameUrl
	 * @param {number} gameId
	 * @param {string} gameToken
	 * @param {string} userId
	 * @param {string} userToken
	 * @param {LowEntryBackend~__authenticationChangedCallback} authenticationChangedCallback (number givenUserId, string givenUserToken, boolean newAuthenticated, number newUserId, string newUserToken)
	 * @param {LowEntryBackend~___executeConnectToServerRequestCallback} callback (boolean success, object error)
	 * @private
	 */
	LowEntryBackend._executeConnectToServerRequest = function(server, gameUrl, gameId, gameToken, userId, userToken, authenticationChangedCallback, callback)
	{
		var data = {};
		{
			data.gameId = gameId;
			data.gameToken = gameToken;
			if(userId !== '')
			{
				data.userId = userId;
				data.userToken = userToken;
			}
		}
		LowEntryBackend._executeRequestBase(gameUrl + '/?/Api/Game/ConnectToServer/', data, function(success, error, result)
		{
			if(!success)
			{
				callback(false, error);
				return;
			}
			try
			{
				var authenticationChanged = LowEntryBackend._getFromApiResponseResult(result, 'authenticationChanged');
				if(authenticationChanged)
				{
					var newAuthenticated = LowEntryBackend._getFromApiResponseResult(result, 'newAuthenticated');
					if(newAuthenticated)
					{
						var newUserId = LowEntryBackend._getFromApiResponseResult(result, 'newUserId');
						var newUserToken = LowEntryBackend._getFromApiResponseResult(result, 'newUserToken');
					}
					var givenUserId = LowEntryBackend._getFromApiResponseResult(result, 'givenUserId');
					var givenUserToken = LowEntryBackend._getFromApiResponseResult(result, 'givenUserToken');
				}
			}
			catch(err)
			{
				callback(false, LowEntryBackend.convertError(err));
				return;
			}
			if(authenticationChanged)
			{
				authenticationChangedCallback(server, givenUserId, givenUserToken, newAuthenticated, newUserId, newUserToken);
			}
			callback(true, null);
		});
	};
	
	/**
	 * @callback LowEntryBackend~___executeApiCallRequestCallback
	 * @param {boolean} success
	 * @param {LowEntryBackend~__error|null} error
	 * @param {object|null} OUT
	 */
	/**
	 * @param {LowEntryBackend} server
	 * @param {string} gameUrl
	 * @param {number} gameId
	 * @param {string} gameToken
	 * @param {string} userId
	 * @param {string} userToken
	 * @param {string} apiCallName
	 * @param {object} IN
	 * @param {LowEntryBackend~__authenticationChangedCallback} authenticationChangedCallback (number givenUserId, string givenUserToken, boolean newAuthenticated, number newUserId, string newUserToken)
	 * @param {LowEntryBackend~___executeApiCallRequestCallback} callback (boolean success, object error, object OUT)
	 * @private
	 */
	LowEntryBackend._executeApiCallRequest = function(server, gameUrl, gameId, gameToken, userId, userToken, apiCallName, IN, authenticationChangedCallback, callback)
	{
		var data = {};
		{
			data.gameId = gameId;
			data.gameToken = gameToken;
			if(userId !== '')
			{
				data.userId = userId;
				data.userToken = userToken;
			}
			data.name = apiCallName;
			data.IN = IN;
		}
		LowEntryBackend._executeRequestBase(gameUrl + '/?/Api/Game/ApiCall/', data, function(success, error, result)
		{
			if(!success)
			{
				callback(false, error, null);
				return;
			}
			try
			{
				var authenticationChanged = LowEntryBackend._getFromApiResponseResult(result, 'authenticationChanged');
				if(authenticationChanged)
				{
					var newAuthenticated = LowEntryBackend._getFromApiResponseResult(result, 'newAuthenticated');
					if(newAuthenticated)
					{
						var newUserId = LowEntryBackend._getFromApiResponseResult(result, 'newUserId');
						var newUserToken = LowEntryBackend._getFromApiResponseResult(result, 'newUserToken');
					}
					var givenUserId = LowEntryBackend._getFromApiResponseResult(result, 'givenUserId');
					var givenUserToken = LowEntryBackend._getFromApiResponseResult(result, 'givenUserToken');
				}
			}
			catch(err)
			{
				callback(false, LowEntryBackend.convertError(err), null);
				return;
			}
			if(authenticationChanged)
			{
				authenticationChangedCallback(server, givenUserId, givenUserToken, newAuthenticated, newUserId, newUserToken);
			}
			try
			{
				var apiCall = LowEntryBackend._getFromApiResponseResult(result, 'apiCall');
				var apiCallResult = LowEntryBackend._parseApiResponseResultApiCall(apiCall);
			}
			catch(err)
			{
				callback(false, LowEntryBackend.convertError(err), null);
				return;
			}
			callback(true, null, apiCallResult);
		});
	};
	
	
	
	
	/**
	 * @param {string} url
	 * @param {string|null|undefined} [filename]
	 */
	LowEntryBackend.downloadFile = function(url, filename = null)
	{
		var $link = $('<a href="" style="display:none" download></a>').appendTo('body');
		$link.attr({
			href:url,
		});
		if(filename)
		{
			$link.attr({
				download:filename,
			});
		}
		$link[0].click();
		$link.remove();
	};
	
	/**
	 * @callback LowEntryBackend~__userSelectFileCallback
	 * @param {File|null} file
	 */
	/**
	 * @callback LowEntryBackend~__userSelectFileErrorCallback
	 * @param {*} error
	 */
	/**
	 * @param {LowEntryBackend~__userSelectFileCallback} callback (File|null file)
	 * @param {LowEntryBackend~__userSelectFileErrorCallback|null|undefined} [optionalOnErrorCallback] (error)
	 * @param {string|null|undefined} [optionalFileFilter]
	 * @param {string|null|undefined} [optionalAdditionalInputHtml]
	 */
	LowEntryBackend.showFileDialog = function(callback, optionalOnErrorCallback = null, optionalFileFilter = null, optionalAdditionalInputHtml = null)
	{
		try
		{
			var uniqueId = function()
			{
				try
				{
					if(typeof framework.uniqueId !== 'undefined')
					{
						return framework.uniqueId();
					}
				}
				catch(e)
				{
				}
				
				var now;
				try
				{
					// noinspection JSDeprecatedSymbols
					now = (performance.timeOrigin || performance.timing.navigationStart) + performance.now();
					if(typeof now !== 'number')
					{
						// noinspection ExceptionCaughtLocallyJS
						throw new Error();
					}
				}
				catch(e)
				{
					now = (Date.now ? Date.now() : (new Date()).getTime());
				}
				return (now + '_' + (Math.random() + '').substring(2)).replace(/\D/g, '_');
			};
			
			
			if(_internal_LowEntryBackend_showFileDialog_lastId !== null)
			{
				$('#' + _internal_LowEntryBackend_showFileDialog_lastId).remove();
			}
			
			_internal_LowEntryBackend_showFileDialog_lastId = 'lowentry_backend_showFileDialog_input_' + uniqueId();
			$('<input id="' + _internal_LowEntryBackend_showFileDialog_lastId + '" multiple type="file" ' + (optionalFileFilter ? ' accept="' + optionalFileFilter + '"' : '') + ' ' + (optionalAdditionalInputHtml ?? '') + ' style="display:none"/>').appendTo('body');
			// noinspection JSJQueryEfficiency
			const $file = $('#' + _internal_LowEntryBackend_showFileDialog_lastId);
			
			$file[0].addEventListener('change', function()
			{
				try
				{
					const files = $file[0].files;
					$file.remove();
					if(files.length <= 0)
					{
						return;
					}
					
					callback(files[0]);
				}
				catch(e)
				{
					console.error(e);
					try
					{
						if(optionalOnErrorCallback)
						{
							optionalOnErrorCallback(e);
						}
					}
					catch(e2)
					{
						console.error(e2);
					}
				}
			});
			$file.trigger('click');
		}
		catch(e)
		{
			console.error(e);
			try
			{
				if(optionalOnErrorCallback)
				{
					optionalOnErrorCallback(e);
				}
			}
			catch(e2)
			{
				console.error(e2);
			}
		}
	};
	
	/**
	 * @param {string|null|undefined} [optionalFileFilter]
	 * @param {string|null|undefined} [optionalAdditionalInputHtml]
	 * @return {Promise<File|null>}
	 */
	LowEntryBackend.showFileDialogPromise = function(optionalFileFilter = null, optionalAdditionalInputHtml = null)
	{
		return new Promise(function(resolve, reject)
		{
			try
			{
				LowEntryBackend.showFileDialog
				(
					function(file)
					{
						resolve(file);
					},
					function(error)
					{
						reject(error);
					},
					optionalFileFilter,
					optionalAdditionalInputHtml,
				);
			}
			catch(e)
			{
				reject(e);
			}
		});
	};
	
	/**
	 * @callback LowEntryBackend~___uploadTickCallback
	 * @return {boolean|undefined}
	 */
	/**
	 * @callback LowEntryBackend~___uploadPercentageChangedCallback
	 * @property {number} percentage
	 * @return {boolean|undefined}
	 */
	/**
	 * @callback LowEntryBackend~___uploadSuccessCallback
	 * @property {object} data
	 * @property {string} textStatus
	 * @property {object} jqXHR
	 */
	/**
	 * @callback LowEntryBackend~___uploadFailedCallback
	 * @property {object} jqXHR
	 * @property {string} textStatus
	 * @property {Error|string} errorThrown
	 */
	/**
	 * @callback LowEntryBackend~___uploadAlwaysCallback
	 * @property {string|object} data|jqXHR
	 * @property {string} textStatus
	 * @property {object|Error|string} jqXHR|errorThrown
	 */
	/**
	 * @typedef {object} LowEntryBackend~__uploadData
	 * @property {string} url
	 * @property {object|string|null|undefined} [data]
	 * @property {LowEntryBackend~___uploadTickCallback|null|undefined} [onTick]
	 * @property {LowEntryBackend~___uploadPercentageChangedCallback|null|undefined} [onPercentageChanged]
	 * @property {LowEntryBackend~___uploadSuccessCallback|null|undefined} [onSuccess]
	 * @property {LowEntryBackend~___uploadFailedCallback|null|undefined} [onFail]
	 * @property {LowEntryBackend~___uploadAlwaysCallback|null|undefined} [onAlways]
	 */
	/**
	 * @param {LowEntryBackend~__uploadData} data
	 */
	LowEntryBackend.uploadFile = function(data)
	{
		if(typeof data.url === 'undefined')
		{
			throw new Error('"url" was not given');
		}
		if(typeof data.data === 'string')
		{
			if(data.data.startsWith('data:'))
			{
				if(typeof window.fetch === 'function')
				{
					window.fetch(data.data)
						.then(function(res)
						{
							return res.blob();
						})
						.then(function(blob)
						{
							data.data = blob;
							LowEntryBackend.uploadFile(data);
						});
					return;
				}
				else
				{
					// can only handle base64 data (not plain data, nor url-safe base64)
					var arr = data.data.split(',');
					var mime = (arr[0].split(';')[0]).substring('data:'.length);
					var bstr = atob(arr[1]);
					var n = bstr.length;
					var u8arr = new Uint8Array(n);
					while(n--)
					{
						u8arr[n] = bstr.charCodeAt(n);
					}
					data.data = new Blob([u8arr], {type:mime});
				}
			}
			else
			{
				throw new Error('invalid "data" was given, should be either a Blob or a data URI, was given a string with value: ' + data.data);
			}
		}
		LowEntryBackend.retryingAjax
		({
				url:        data.url,
				type:       'PUT',
				dataType:   'text',
				data:       data.data || '',
				cache:      false,
				processData:false,
				xhr:        LowEntryBackend.xhrUploadProgressFunction(data.onTick || null, data.onPercentageChanged || null),
			},
			data.onSuccess || null,
			data.onFail || null,
			data.onAlways || null,
		);
	};
	
	/**
	 * @param {LowEntryBackend~__uploadData} data
	 * @return {Promise<*>}
	 */
	LowEntryBackend.uploadFilePromise = function(data)
	{
		return new Promise(function(resolve, reject)
		{
			try
			{
				var onSuccess = data['onSuccess'];
				data['onSuccess'] = function(_data, _textStatus, _jqXHR)
				{
					try
					{
						if(onSuccess)
						{
							onSuccess(_data, _textStatus, _jqXHR);
						}
					}
					catch(e)
					{
						console.error(e);
					}
					resolve(_data);
				};
				
				var onFail = data['onFail'];
				data['onFail'] = function(_jqXHR, _textStatus, _errorThrown)
				{
					try
					{
						if(onFail)
						{
							onFail(_jqXHR, _textStatus, _errorThrown);
						}
					}
					catch(e)
					{
						console.error(e);
					}
					reject(_errorThrown);
				};
				
				LowEntryBackend.uploadFile(data);
			}
			catch(e)
			{
				reject(e);
			}
		});
	};
	
	
	/**
	 * @param {object} data
	 * @param {LowEntryBackend~___uploadSuccessCallback|function|null|undefined} [callbackSuccess]
	 * @param {LowEntryBackend~___uploadFailedCallback|function|null|undefined} [callbackFailed]
	 * @param {LowEntryBackend~___uploadAlwaysCallback|function|null|undefined} [callbackAlways]
	 * @private
	 */
	LowEntryBackend.retryingAjax = function(data, callbackSuccess, callbackFailed, callbackAlways)
	{
		if(typeof callbackSuccess !== 'function')
		{
			callbackSuccess = function()
			{
			};
		}
		if(typeof callbackFailed !== 'function')
		{
			callbackFailed = function()
			{
			};
		}
		if(typeof callbackAlways !== 'function')
		{
			callbackAlways = function()
			{
			};
		}
		
		if(!('customRetries' in data))
		{
			data.customRetries = 10;
		}
		if(!('customRetryTimeout' in data))
		{
			data.customRetryTimeout = 1000;
		}
		
		$.ajax(data)
			.done(function(data_, textStatus, jqXHR)
			{
				try
				{
					callbackSuccess(data_, textStatus, jqXHR);
				}
				catch(e)
				{
					console.error(e);
				}
			})
			.fail(function(jqXHR, textStatus, errorThrown)
			{
				// noinspection EqualityComparisonWithCoercionJS
				if((data.customRetries > 0) && (jqXHR.status == 503))
				{
					setTimeout
					(
						function()
						{
							data.customRetries--;
							data.customRetryTimeout *= 2;
							LowEntryBackend.retryingAjax(data, callbackSuccess, callbackFailed, callbackAlways);
						},
						data.customRetryTimeout,
					);
				}
				else
				{
					try
					{
						callbackFailed(jqXHR, textStatus, errorThrown);
					}
					catch(e)
					{
						console.error(e);
					}
				}
			})
			.always(function(data_jqXHR, textStatus, jqXHR_errorThrown)
			{
				try
				{
					callbackAlways(data_jqXHR, textStatus, jqXHR_errorThrown);
				}
				catch(e)
				{
					console.error(e);
				}
			});
	};
	
	/**
	 * @param {LowEntryBackend~___uploadTickCallback|null|undefined} [callbackTick]
	 * @param {LowEntryBackend~___uploadPercentageChangedCallback|function|null|undefined} [callbackPercentageChanged]
	 * @private
	 */
	LowEntryBackend.xhrUploadProgressFunction = function(callbackTick, callbackPercentageChanged)
	{
		return function()
		{
			var xhr = $.ajaxSettings.xhr();
			if(xhr.upload)
			{
				var lastPercentage = -1.00;
				xhr.upload.addEventListener('progress', function(event)
				{
					if(typeof callbackTick === 'function')
					{
						try
						{
							if(callbackTick() === false)
							{
								try
								{
									xhr.abort();
								}
								catch(e)
								{
								}
								return;
							}
						}
						catch(e)
						{
							console.error(e);
						}
					}
					
					var position = event.loaded || event.position;
					var total = event.total;
					if(event.lengthComputable)
					{
						var percentage = Math.ceil((position / total) * 100.0);
						if(lastPercentage !== percentage)
						{
							lastPercentage = percentage;
							if(typeof callbackPercentageChanged === 'function')
							{
								try
								{
									if(callbackPercentageChanged(percentage) === false)
									{
										try
										{
											xhr.abort();
										}
										catch(e)
										{
										}
										return;
									}
								}
								catch(e)
								{
									console.error(e);
								}
							}
						}
					}
				}, true);
			}
			return xhr;
		};
	};
	
	
	
	
	/**
	 * @param {boolean|null|undefined} [returnNull]
	 * @param {boolean|null|undefined} [forceRecreate]
	 * @return {WebSocket}
	 * @private
	 */
	LowEntryBackend.prototype._websocketGetConnection = function(returnNull, forceRecreate)
	{
		var THIS = this;
		
		var connection = THIS._websocketConnection;
		if(((connection !== null) || (returnNull === true)) && (forceRecreate !== true))
		{
			return connection;
		}
		connection = new WebSocket('wss://' + THIS._getGameUrlHostName() + ':7784');
		connection.binaryType = 'arraybuffer';
		THIS._websocketConnection = connection;
		
		connection.onerror = function(e)
		{
			console.error(e);
			connection.close();
		};
		
		connection.onclose = function()
		{
			THIS._websocketConnectionIsOpen = false;
			THIS._websocketSendBuffer = [];
			THIS._websocketListeningChannels = {};
			setTimeout(function()
			{
				var connection = THIS._websocketConnection;
				if(connection !== null)
				{
					THIS._websocketGetConnection(false, true);
				}
			}, 500);
		};
		
		connection.onopen = function()
		{
			THIS._websocketConnectionIsOpen = true;
			
			LowEntryBackend._each(THIS._websocketListeners, function(key, listenerArray)
			{
				LowEntryBackend._each(listenerArray, function(index, listener)
				{
					if(listener['_success'] !== false)
					{
						listener['_buffer'] = [];
						listener['_success'] = null;
					}
				});
				THIS._websocketSendJoinChannel(key);
			});
		};
		
		connection.onmessage = function(event)
		{
			var key;
			var message;
			try
			{
				var bytes = new Uint8Array(event.data);
				var i = 0;
				
				if(bytes.length <= 0)
				{
					return;
				}
				
				if(bytes[i] !== 1)// 1 = BROADCAST
				{
					console.error('Broadcast connection has received an invalid packet, packet didn\'t start with 0x01');
					return;
				}
				i += 1;
				
				{// channel >>
					if(i + 4 > bytes.length)
					{
						console.error('Broadcast connection has received an invalid packet, packet contains too little bytes for the channel bytes length');
						return;
					}
					var keyBytesLength = LowEntryBackend._bytesToInt32(bytes, i);
					i += 4;
					
					if(i + keyBytesLength > bytes.length)
					{
						console.error('Broadcast connection has received an invalid packet, packet contains too little bytes for the channel string');
						return;
					}
					key = LowEntryBackend._bytesToStringUtf8(bytes, i, keyBytesLength);// 's' + gameId + '_' + channel
					i += keyBytesLength;
					
					var channelStringDelimIndex = key.indexOf('_');
					if(channelStringDelimIndex < 0)
					{
						console.error('Broadcast connection has received an invalid packet, delimiter \'_\' not found in the channel string: ' + key);
						return;
					}
					//var type = channelString.substring(0, 1);// 's' / 'c' / etc (normal broadcast, client broadcast, etc)
					var _gameId = key.substring(1, channelStringDelimIndex);
					if(_gameId !== ('' + THIS.gameId))
					{
						console.error('Broadcast connection has received an invalid packet, game ID doesn\'t match');
						return;
					}
				}// channel <<
				
				{// message >>
					if(i + 4 > bytes.length)
					{
						console.error('Broadcast connection has received an invalid packet, packet contains too little bytes for the message bytes length');
						return;
					}
					var messageBytesLength = LowEntryBackend._bytesToInt32(bytes, i);
					i += 4;
					
					if(i + messageBytesLength > bytes.length)
					{
						console.error('Broadcast connection has received an invalid packet, packet contains too little bytes for the message json string');
						return;
					}
					message = LowEntryBackend._bytesToStringUtf8(bytes, i, messageBytesLength);
					
					message = JSON.parse(message);
					if(!LowEntryBackend._hasIndex(message, 'result'))
					{
						console.error('Broadcast connection has received an invalid packet, the response didn\'t contain a result field');
						return;
					}
					message = message['result'];
				}// message <<
			}
			catch(e)
			{
				console.error('Broadcast connection has received an invalid packet');
				console.error(e);
				return;
			}
			
			if((typeof THIS._websocketListeners[key] === 'undefined') || (THIS._websocketListeners[key] === null))
			{
				return;
			}
			var websocketListenersClone = THIS._websocketListeners[key].slice(0);
			LowEntryBackend._each(websocketListenersClone, function(index, listener)
			{
				if(listener['_success'] === true)
				{
					THIS._websocketRunMessageCallback(listener, message);
				}
				else if(listener['_success'] === null)
				{
					if(listener['_buffer'] !== null)
					{
						listener['_buffer'].push(message);
					}
				}
			});
		};
		
		return connection;
	};
	
	/**
	 * @callback LowEntryBackend~___websocketSendCallback
	 * @param {WebSocket} connection
	 */
	/**
	 * @param {LowEntryBackend~___websocketSendCallback|[]} functionOrBytes
	 * @private
	 */
	LowEntryBackend.prototype._websocketSend = function(functionOrBytes)
	{
		try
		{
			if(this._websocketConnectionIsOpen === true)
			{
				this._websocketSendBuffer.push(functionOrBytes);
			}
		}
		catch(e)
		{
			console.error(e);
		}
	};
	
	/**
	 * @param {WebSocket} connection
	 * @param {[]} bytes
	 * @private
	 */
	LowEntryBackend.prototype._websocketSend_internal = function(connection, bytes)
	{
		try
		{
			if(this._websocketConnectionIsOpen === true)
			{
				var buffer = new Uint8Array(bytes).buffer;
				connection.send(buffer);
			}
		}
		catch(e)
		{
			console.error(e);
		}
	};
	
	/**
	 * @return boolean
	 * @private
	 */
	LowEntryBackend.prototype._websocketSendLoop = function()
	{
		if(this._websocketConnectionIsOpen !== true)
		{
			return false;
		}
		
		if(this._websocketSendBuffer.length <= 0)
		{
			return false;
		}
		
		var connection = this._websocketGetConnection(true);
		if(connection === null)
		{
			return false;
		}
		
		if(connection.bufferedAmount > 0)
		{
			return false;
		}
		
		try
		{
			var functionOrBytes = this._websocketSendBuffer.shift();
			if(typeof functionOrBytes === 'function')
			{
				functionOrBytes(connection);
			}
			else
			{
				this._websocketSend_internal(connection, functionOrBytes);
			}
		}
		catch(e)
		{
			console.error(e);
		}
		return true;
	};
	
	/**
	 * @param {string} key
	 * @param {object|undefined|null} [listener]
	 * @private
	 */
	LowEntryBackend.prototype._websocketRunInitCallback = function(key, listener)
	{
		var THIS = this;
		
		if((typeof listener === 'undefined') || (listener === null) || (listener['_success'] === false))
		{
			return;
		}
		
		var firstRun = (listener['_firstRun'] === true);
		if(firstRun)
		{
			listener['_firstRun'] = false;
		}
		
		if(listener['init'] === null)
		{
			if(listener['_success'] !== false)
			{
				listener['_success'] = true;
			}
			if(listener['_buffer'] !== null)
			{
				LowEntryBackend._each(listener['_buffer'], function(index, message)
				{
					if(listener['_success'] === true)
					{
						THIS._websocketRunMessageCallback(listener, message);
					}
				});
				listener['_buffer'] = null;
			}
			return;
		}
		
		try
		{
			var promise = new Promise(function(resolve, reject)
			{
				try
				{
					listener['init'](resolve, reject, firstRun);
				}
				catch(e)
				{
					console.error('Error in initializer callback:');
					console.error(e);
					reject(e);
				}
			});
			promise.then
			(
				function(value)// success
				{
					if(listener['_success'] !== false)
					{
						listener['_success'] = true;
					}
					if(listener['_buffer'] !== null)
					{
						LowEntryBackend._each(listener['_buffer'], function(index, message)
						{
							if(listener['_success'] === true)
							{
								THIS._websocketRunMessageCallback(listener, message);
							}
						});
						listener['_buffer'] = null;
					}
				},
				function(reason)// error
				{
					listener['_success'] = false;
					listener['_buffer'] = null;
					THIS._websocketOffBroadcast(key, listener);
				},
			);
		}
		catch(e)
		{
			// in case Promises aren't supported for example
			console.error('Error when trying to run the initializer callback:');
			console.error(e);
			listener['_success'] = false;
			listener['_buffer'] = null;
			THIS._websocketOffBroadcast(key, listener);
		}
	};
	
	/**
	 * @param {object} listener
	 * @param {*} message
	 * @private
	 */
	LowEntryBackend.prototype._websocketRunMessageCallback = function(listener, message)
	{
		if(listener['message'] === null)
		{
			return;
		}
		try
		{
			listener['message'](message);
		}
		catch(e)
		{
			console.error('Error in onMessageReceived callback:');
			console.error(e);
		}
	};
	
	/**
	 * @param {string} key
	 * @param {LowEntryBackend~__onBroadcastOnMessageReceivedCallback} onMessageReceivedCallback
	 * @param {LowEntryBackend~__onBroadcastInitializerCallback|null|undefined} [initializerCallback]
	 * @return {object}
	 * @private
	 */
	LowEntryBackend.prototype._websocketOnBroadcast = function(key, onMessageReceivedCallback, initializerCallback)
	{
		key = '' + key;
		var connection = this._websocketGetConnection();
		if(typeof initializerCallback === 'undefined')
		{
			initializerCallback = null;
		}
		
		var listener = {'message':onMessageReceivedCallback, 'init':initializerCallback, '_buffer':[], '_success':null, '_firstRun':true};
		if(typeof this._websocketListeners[key] === 'undefined')
		{
			this._websocketListeners[key] = [listener];
		}
		else
		{
			this._websocketListeners[key].push(listener);
		}
		
		this._websocketSendJoinChannel(key, listener);
		
		// noinspection JSValidateTypes
		return listener;
	};
	
	/**
	 * @param {string} key
	 * @param {object|undefined|null} [listener]
	 * @private
	 */
	LowEntryBackend.prototype._websocketSendJoinChannel = function(key, listener)
	{
		var THIS = this;
		this._websocketSend(function(connection)
		{
			if(typeof THIS._websocketListeners[key] === 'undefined')
			{
				return;
			}
			if(THIS._websocketListeningChannels[key] === true)
			{
				THIS._websocketRunInitCallback(key, listener);
				return;
			}
			var stringBytes = LowEntryBackend._stringToBytesUtf8(key);
			THIS._websocketSend_internal(connection, [].concat(LowEntryBackend._uint8ToBytes(4), LowEntryBackend._int32ToBytes(stringBytes.length), stringBytes));// 4 = JOIN
			THIS._websocketListeningChannels[key] = true;
			
			var listenerArrayClone = THIS._websocketListeners[key].slice(0);
			LowEntryBackend._each(listenerArrayClone, function(index, listener)
			{
				THIS._websocketRunInitCallback(key, listener);
			});
		});
	};
	
	/**
	 * @param {string} key
	 * @param {object|null|undefined} [listener]
	 * @private
	 */
	LowEntryBackend.prototype._websocketOffBroadcast = function(key, listener)
	{
		key = '' + key;
		var connection = this._websocketGetConnection(true);
		if(connection === null)
		{
			return;
		}
		if(typeof this._websocketListeners[key] === 'undefined')
		{
			return;
		}
		if(typeof listener === 'undefined')
		{
			listener = null;
		}
		if(listener !== null)
		{
			while(true)
			{
				var index = this._websocketListeners[key].indexOf(listener);
				if(index <= -1)
				{
					return;
				}
				var deleted = this._websocketListeners[key].splice(index, 1);
				LowEntryBackend._each(deleted, function(index, listener)
				{
					listener['_success'] = false;
					listener['_buffer'] = null;
				});
				if(this._websocketListeners[key].length === 0)
				{
					listener = null;
					break;
				}
			}
		}
		if(listener === null)
		{
			LowEntryBackend._each(this._websocketListeners[key], function(index, listener)
			{
				listener['_success'] = false;
				listener['_buffer'] = null;
			});
			delete this._websocketListeners[key];
			var THIS = this;
			this._websocketSend(function(connection)
			{
				var stringBytes = LowEntryBackend._stringToBytesUtf8(key);
				THIS._websocketSend_internal(connection, [].concat(LowEntryBackend._uint8ToBytes(3), LowEntryBackend._int32ToBytes(stringBytes.length), stringBytes));// 3 = LEAVE
				delete THIS._websocketListeningChannels[key];
			});
		}
	};
	
	/**
	 * @param {string} keyStartsWith
	 * @private
	 */
	LowEntryBackend.prototype._websocketOffAllBroadcasts = function(keyStartsWith)
	{
		var connection = this._websocketGetConnection(true);
		if(connection === null)
		{
			return;
		}
		
		if((typeof keyStartsWith === 'undefined') || (keyStartsWith === null))
		{
			LowEntryBackend._each(this._websocketListeners, function(key, listenerArray)
			{
				LowEntryBackend._each(listenerArray, function(index, listener)
				{
					listener['_success'] = false;
					listener['_buffer'] = null;
				});
			});
			this._websocketListeners = {};
			var THIS = this;
			this._websocketSend(function(connection)
			{
				THIS._websocketSend_internal(connection, LowEntryBackend._uint8ToBytes(2));// 2 = LEAVE ALL
				THIS._websocketListeningChannels = {};
			});
			return;
		}
		
		keyStartsWith = '' + keyStartsWith;
		var websocketListenersClone = this._websocketListeners.slice(0);
		LowEntryBackend._each(websocketListenersClone, function(key, listenerArray)
		{
			if(key.startsWith(keyStartsWith))
			{
				this._websocketOffBroadcast(key);
			}
		});
	};
	
	
	
	
	/**
	 * @param {number} value
	 * @return {[]}
	 * @private
	 */
	LowEntryBackend._uint8ToBytes = function(value)
	{
		return [value & 0xFF];
	};
	
	/**
	 * @param {[]|Uint8Array} bytes
	 * @param {number} [index]
	 * @return {number}
	 * @private
	 */
	LowEntryBackend._bytesToUint8 = function(bytes, index)
	{
		if((typeof index === 'undefined') || (index === null))
		{
			index = 0;
		}
		return (bytes[index] & 0xFF);
	};
	
	/**
	 * @param {number} value
	 * @return {[]}
	 * @private
	 */
	LowEntryBackend._int32ToBytes = function(value)
	{
		return [(value >> 24) & 0xFF, (value >> 16) & 0xFF, (value >> 8) & 0xFF, value & 0xFF];
	};
	
	/**
	 * @param {[]|Uint8Array} bytes
	 * @param {number} [index]
	 * @return {number}
	 * @private
	 */
	LowEntryBackend._bytesToInt32 = function(bytes, index)
	{
		if((typeof index === 'undefined') || (index === null))
		{
			index = 0;
		}
		return ((bytes[index] & 0xFF) << 24) | ((bytes[index + 1] & 0xFF) << 16) | ((bytes[index + 2] & 0xFF) << 8) | (bytes[index + 3] & 0xFF);
	};
	
	/**
	 * @param {string} value
	 * @return {[]}
	 * @private
	 */
	LowEntryBackend._stringToBytesUtf8 = function(value)
	{
		var bytes = [];
		for(var i = 0, j = value.length; i < j; i++)
		{
			var charCode = value[i].codePointAt(0);
			if(charCode < 128)
			{
				bytes.push(charCode);
				continue;
			}
			
			var neededBytes;
			if(charCode < 2048)
			{
				neededBytes = 2;
			}
			else if(charCode < 65536)
			{
				neededBytes = 3;
			}
			else if(charCode < 2097152)
			{
				neededBytes = 4;
			}
			else
			{
				throw new Error('CharCode ' + charCode + ' cannot be encoded with UTF-8');
			}
			
			bytes.push((parseInt('1111'.slice(0, neededBytes), 2) << (8 - neededBytes)) + (charCode >>> ((neededBytes - 1) * 6)));
			neededBytes--;
			while(neededBytes > 0)
			{
				neededBytes--;
				bytes.push(((charCode >>> (neededBytes * 6)) & 0x3f) | 0x80);
			}
		}
		return bytes;
	};
	
	/**
	 * @param {[]|Uint8Array} bytes
	 * @param {number} [index]
	 * @param {number} [length]
	 * @return {string}
	 * @private
	 */
	LowEntryBackend._bytesToStringUtf8 = function(bytes, index, length)
	{
		if((typeof index === 'undefined') || (index === null))
		{
			index = 0;
		}
		if((typeof length === 'undefined') || (length === null))
		{
			length = bytes.length;
		}
		else
		{
			length += index;
			if(length > bytes.length)
			{
				length = bytes.length;
			}
		}
		var chars = [];
		while(index < length)
		{
			var charLength;
			var theByte = bytes[index];
			if((theByte & 0xf0) === 0xf0)// got all 1 bits: 11110000
			{
				charLength = 4;
			}
			else if((theByte & 0xe0) === 0xe0)// got all 1 bits: 11100000
			{
				charLength = 3;
			}
			else if((theByte & 0xc0) === 0xc0)// got all 1 bits: 11000000
			{
				charLength = 2;
			}
			else if((theByte & 0x7f) === theByte)// doesn't got the 1 bit: 10000000
			{
				charLength = 1;
			}
			else
			{
				throw Error('Index ' + index + ': Invalid UTF-8 byte: ' + theByte);
			}
			
			var charCode;
			if(charLength === 1)
			{
				charCode = bytes[index];
				index++;
			}
			else
			{
				if(index + charLength > length)
				{
					throw Error('Index ' + index + ': Found a ' + charLength + ' bytes encoded char declaration but only ' + (length - index) + ' bytes are available');
				}
				var mask = '00000000'.slice(0, charLength) + 1 + '00000000'.slice(charLength + 1);
				var invalidUtf8 = bytes[index] & parseInt(mask, 2);
				if(invalidUtf8)
				{
					throw Error('Index ' + index + ': A ' + charLength + ' bytes encoded char cannot encode the ' + (charLength + 1) + 'th rank bit to 1');
				}
				mask = '0000'.slice(0, charLength + 1) + '11111111'.slice(charLength + 1);
				charCode = (bytes[index] & parseInt(mask, 2)) << ((charLength - 1) * 6);
				charLength--;
				while(charLength > 0)
				{
					charLength--;
					index++;
					if(((bytes[index] & 0x80) !== 0x80) || ((bytes[index] & 0x40) === 0x40))
					{
						throw Error('Index ' + index + ': Next bytes of encoded char must begin with a "10" bit sequence');
					}
					charCode += (bytes[index] & 0x3f) << (charLength * 6);
				}
				index++;
			}
			chars.push(String.fromCodePoint(charCode));
		}
		return chars.join('');
	};
	
	
	return LowEntryBackend;
})();
