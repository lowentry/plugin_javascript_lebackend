declare namespace LowEntryBackend
{
	const PRODUCTION_SERVER: string;
	const DEVELOPMENT_SERVER: string;
	
	const ErrorCode: {
		OTHER: number,
		CUSTOM_ERROR: number,
		MISSING_DATA: number,
		INVALID_DATA: number,
		INVALID_CODE: number,
		INVALID_SERVER_TOKEN: number,
		SERVER_DISABLED: number,
		SERVER_UNAVAILABLE: number,
		SERVER_TEMPORARILY_UNAVAILABLE: number,
		USER_UNAUTHORIZED_ACCESS: number,
		USER_LOGIN_USERNAME_DOESNT_EXIST: number,
		USER_LOGIN_INCORRECT_PASSWORD: number,
		USER_REGISTER_USERNAME_ALREADY_IN_USE: number,
		USER_REGISTER_USERNAME_INVALID: number,
		USER_REGISTER_EMAIL_INVALID: number,
	};
	
	function createError(message: string, code: number, trace?: any[] | null): LowEntryBackend.Error;
	function convertError(err: any | LowEntryBackend.Error): LowEntryBackend.Error;
	function connectToServer(mainServerUrl: string, gameId: number, gameToken: string, callback: (success: boolean, error: LowEntryBackend.Error, server: LowEntryBackend.Server) => void): void;
	function connectToServerPromise(mainServerUrl: string, gameId: number, gameToken: string): Promise<any>;
	function downloadFile(url: string, filename?: string | null): void;
	function showFileDialog(callback: (file: File | null) => void, optionalOnErrorCallback?: ((error: any) => void) | null, optionalFileFilter?: string | null, optionalAdditionalInputHtml?: string | null): void;
	function showFileDialogPromise(optionalFileFilter?: string | null, optionalAdditionalInputHtml?: string | null): Promise<File | null>;
	function uploadFile(data: {
		url: string,
		data?: Blob | string | null,
		onTick?: (() => boolean | void) | null,
		onPercentageChanged?: ((percentage: number) => boolean | void) | null,
		onSuccess?: ((data: any, textStatus: string, jqXHR: any) => void) | null,
		onFail?: ((jqXHR: any, textStatus: string, errorThrown: string) => void) | null,
		onAlways?: ((data_or_jqXHR: string | any, textStatus: string, jqXHR_or_errorThrown: any | string) => void) | null,
	}): void;
	function uploadFilePromise(data: {
		url: string,
		data?: Blob | string | null,
		onTick?: (() => boolean | void) | null,
		onPercentageChanged?: ((percentage: number) => boolean | void) | null,
		onSuccess?: ((data: any, textStatus: string, jqXHR: any) => void) | null,
		onFail?: ((jqXHR: any, textStatus: string, errorThrown: string) => void) | null,
		onAlways?: ((data_or_jqXHR: string | any, textStatus: string, jqXHR_or_errorThrown: any | string) => void) | null,
	}): Promise<any>;
	
	
	// ----------------------
	
	
	class Server
	{
		public mainServerUrl: string;
		public cookiePrefix: string | null;
		public gameUrl: string;
		public gameId: number;
		public gameToken: string;
		public userId: string;
		public userToken: string;
		
		public isAuthenticated(): boolean;
		public clearAuthentication(): void;
		public authenticate(userId: string, userToken: string): void;
		public getAuthenticatedUserId(): string;
		public getAuthenticatedUserToken(): string;
		public getAuthenticatedUserData(): { userId: string, userToken: string };
		public apiCall(name: string, IN: any, callback: (success: boolean, error: LowEntryBackend.Error, OUT: LowEntryBackend.Data) => void): void;
		public apiCallPromise(name: string, IN?: any): Promise<any>;
		public onbroadcast(channel: string, onMessageReceivedCallback: (message: any) => void, initializerCallback?: ((resolve: ((value?: any) => void), reject: ((reason?: any) => void), firstRun?: boolean) => void) | null): LowEntryBackend.RemovableListener;
		public offbroadcast(channel: string): void;
		public offbroadcastall(): void;
		public onclientbroadcast(channel: string, onMessageReceivedCallback: (message: any) => void, initializerCallback?: ((resolve: ((value?: any) => void), reject: ((reason?: any) => void), firstRun?: boolean) => void) | null): LowEntryBackend.RemovableListener;
		public offclientbroadcast(channel: string): void;
		public offclientbroadcastall(): void;
		public clientbroadcast(channel: string, message: any): void;
		public onauthchange(callback: (server?: LowEntryBackend.Server) => void): LowEntryBackend.RemovableListener;
		public offauthchangeall(): void;
	}
	
	
	type Error = { name: string, message: string, code: number, trace: any[] };
	
	
	class RemovableListener
	{
		remove(): void;
	}
	
	
	interface Data
	{
		[name: string]: LowEntryBackend.Data | any
	}
}
