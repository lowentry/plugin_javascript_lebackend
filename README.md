**Low Entry Backend Javascript** is a deprecated **Javascript library** for the deprecated [LE Backend][1] service.

See the `tests` folder for examples on how to use this plugin. 

It requires the following libraries to be included before this one:

- jquery
- js-cookie

[1]: https://backend.lowentry.com/
[2]: https://packagist.org/packages/lowentry/javascript-framework
